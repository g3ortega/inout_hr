Rails.application.routes.draw do
  devise_for :employees

  root 'pages#index'

  namespace :api, defaults: {format: :json} do
    namespace :v1 do
      resource :sessions, only: %i[show create destroy]

      resources :branches do
        resources :employees do
          resources :schedule_logs
          post :clock_in
          post :clock_out
          post :got_sick
          post :recovered
          post :went_on_vacation
        end

        resource :reports do
          get :whos_working
          get :schedule_logs_by_period
        end
      end
    end
  end

  get '/dashboard', to: 'pages#index'
  get '/dashboard/*path', to: 'pages#index'

  mount SwaggerUiEngine::Engine, at: "/api_docs"
end
