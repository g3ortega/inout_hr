import ReactOnRails from 'react-on-rails';

import Index from '../bundles/Inout/components/Index';

// This is how react_on_rails can see the Inout in the browser.
ReactOnRails.register({
  Index,
});
