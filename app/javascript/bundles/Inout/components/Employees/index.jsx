import Collection from './Collection';
import New from './New';
import Show from './Show';
import Edit from './Edit';

export default {
  Collection,
  New,
  Show,
  Edit
};