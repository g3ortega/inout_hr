import React from 'react';
import Api from "../../helpers/api";
import EmployeeStateActions from "./EmployeeStateActions";

class Show extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      employee: null
    };

    this.onEventTransition = this.onEventTransition.bind(this);
    this.fetchEmployee = this.fetchEmployee.bind(this);
  }

  componentDidMount() {
    this.fetchEmployee()
  }

  onEventTransition() {
    this.fetchEmployee()
  }

  async fetchEmployee() {
    const response = await Api.get(`/branches/${this.props.match.params.branch_id}/employees/${this.props.match.params.employee_id}`);
    const status = await response.status;

    switch (status) {
      case 200:
        const body = await response.json();
        this.setState({
          employee: body.data.attributes
        });
        break;
      case 401:
        break;
    }
  }

  render() {
    return (
      <div className="container-fluid mt--7">
        <div className="row">
          <div className="col">
            <div className="card shadow">
              <div className="card-header border-0">
                <div className="row align-items-center">
                  <div className="col-4">
                    <h3 className="mb-0">Employee {this.state.employee && this.state.employee.name}</h3>

                    <h2 className="lead">Logged Time This Week: {this.state.employee && this.state.employee.current_week_time_tracked}</h2>
                  </div>
                  <div className="col-12 text-right">
                  </div>
                </div>
              </div>
              <div className="col-12">

              </div>
            </div>
          </div>
        </div>

        <div className="row mt-5">
          <div className="col-xl-8 mb-5 mb-xl-0">
            <div className="card shadow">
              <div className="card-header">
                <h3>Employee Details</h3>
              </div>
              {
                this.state.employee &&
                <div className="EmployeeDetails card-body">
                  <p><b>Name: </b> {this.state.employee.name}</p>
                  <p><b>Address: </b> {this.state.employee.address}</p>
                  <p><b>Phone Number: </b> {this.state.employee.phone_number}</p>
                  <p><b>Emergency Number: </b> {this.state.employee.phone_number}</p>
                  <p><b>Position: </b> {this.state.employee.position}</p>
                </div>
              }
            </div>
          </div>

          <div className="col-xl-4">
            <div className="card shadow">
              <div className="card-header">
                <h3>Status</h3>
              </div>

              <div className="card-body">
                <EmployeeStateActions employee={this.state.employee}
                                      branch_id={this.props.match.params.branch_id}
                                      onEventTransition={this.onEventTransition}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Show;