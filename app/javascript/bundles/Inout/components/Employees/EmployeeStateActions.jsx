import React from 'react';
import Api from '../../helpers/api';
import moment from 'moment';
import Timer from "react-compound-timer";

class EmployeeStateActions extends React.Component {
  constructor(props) {
    super(props);
    this.triggerEvent = this.triggerEvent.bind(this);
  }

  render() {
    const now = new Date();

    return (
      <React.Fragment>
        <h1 className="text-center">{this.props.employee && this.props.employee.state}</h1>

        {this.props.employee && this.props.employee.aasm_state === 'out' &&
          <div className="text-center">
            <a onClick={(e) => this.triggerEvent(e, this.props.employee, 'clock_in')} className='btn btn-info'>Clock in</a>
          </div>
        }

        {this.props.employee && this.props.employee.aasm_state === 'working' &&
          <div className="text-center">
            <h3>Current Time Being Logged:</h3>
            <Timer initialTime={parseInt(moment.duration(moment(now).diff(this.props.employee.updated_at)).asSeconds()) * 1000} lastUnit="h">
                <React.Fragment>
                  <div className="d-inline bg-success mr-1"><Timer.Hours /> <b>hours</b></div>
                  <div className="d-inline bg-success mr-1"><Timer.Minutes /> <b>minutes</b></div>
                  <div className="d-inline bg-success"><Timer.Seconds /> <b>seconds</b></div>
                </React.Fragment>
            </Timer>
            <a onClick={(e) => this.triggerEvent(e, this.props.employee, 'clock_out')} className='btn btn-info'>Clock out</a>
          </div>
        }
      </React.Fragment>
    );
  }

  async triggerEvent(e, employee, event_type) {
    e.preventDefault();

    const response = await Api.post(`/branches/${this.props.branch_id}/employees/${employee.id}/${event_type}`);
    const status = await response.status;

    if(status === 200) {
      this.props.onEventTransition();
    }
  }

}

export default EmployeeStateActions;