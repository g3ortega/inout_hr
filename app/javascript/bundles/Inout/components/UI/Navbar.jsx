import React from "react";
import {inject, observer} from "mobx-react";
import e from '../Employees';
import {NavLink} from "react-router-dom";
import {Route} from "react-router";

@inject('employee') @observer
export default class Navbar extends React.Component {
  constructor(props) {
    super(props);
    this.signOut = this.signOut.bind(this);
  }

  signOut(e) {
    e.preventDefault();
    this.props.employee.destroySession();
  }

  render() {
    const { employee } = this.props;

    return(
      <nav className="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
        <div className="container-fluid">
          <NavLink to='/dashboard' className="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block">
            Dashboard
          </NavLink>

          <ul className="navbar-nav align-items-center d-none d-md-flex">
            <li className="nav-item dropdown">
              <a className="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                 aria-expanded="false">
                <div className="media align-items-center">
                  <div className="media-body ml-2 d-none d-lg-block">
                    <span className="mb-0 text-sm  font-weight-bold">{employee && employee.current_employee && employee.current_employee.attributes.name}</span>
                  </div>
                </div>
              </a>
              <div className="dropdown-menu dropdown-menu-arrow dropdown-menu-right">

                <NavLink to='/dashboard/settings' className="dropdown-item" >
                  <i className="ni ni-settings" /> Settings
                </NavLink>

                <a className="dropdown-item" href='' onClick={this.signOut}>
                  <i className="ni ni-user-run" /> Logout
                </a>
              </div>
            </li>
          </ul>
        </div>
      </nav>
    )
  }
}