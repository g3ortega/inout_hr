import React from "react";
import {inject, observer} from "mobx-react";
import {NavLink} from "react-router-dom";

@inject('employee') @observer
export default class Sidebar extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {employee} = this.props;

    return (
      <nav className="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
        <div className="container-fluid">
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main"
                  aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"/>
          </button>

          <NavLink to='/dashboard' className="navbar-brand pt-0">
            <h1>
              InOut
            </h1>
          </NavLink>

          <ul className="nav align-items-center d-md-none">

            <li className="nav-item dropdown">
              <a className="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                 aria-expanded="false">
                <div className="media align-items-center">
                  <div className="media-body ml-2 d-none d-lg-block">
                    <span className="mb-0 text-sm  font-weight-bold">Gerardo Ortega</span>
                  </div>
                </div>
              </a>
              <div className="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                <a className="dropdown-item" href="/users/edit">
                  <i className="ni ni-settings" /> Settings
                </a> <a className="dropdown-item" rel="nofollow" data-method="delete" href="/users/sign_out">
                <i className="ni ni-user-run" /> Logout
              </a></div>
            </li>
          </ul>

          <div className="collapse navbar-collapse" id="sidenav-collapse-main">

            <div className="navbar-collapse-header d-md-none">
              <div className="row">
                <div className="col-6 collapse-brand">
                  <a href="/">
                    <h1>LandingCrew</h1>
                  </a>
                </div>
                <div className="col-6 collapse-close">
                  <button type="button" className="navbar-toggler" data-toggle="collapse"
                          data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false"
                          aria-label="Toggle sidenav">
                    <span />
                    <span />
                  </button>
                </div>
              </div>
            </div>

            <ul className="navbar-nav">
              {employee && employee.current_employee &&
                <React.Fragment>
                  <li className="nav-item">
                    <NavLink className="nav-link" to={`/dashboard/branches/${employee.current_employee.attributes.branch.id}`}>
                      <i className="ni ni-building"/> {employee.current_employee.attributes.branch.name}
                    </NavLink>
                  </li>

                  <li className="nav-item">
                    <NavLink className="nav-link" to={`/dashboard/working/${employee.current_employee.attributes.branch.id}`}>
                      <i className="ni ni-square-pin"/> Who's Working
                    </NavLink>
                  </li>

                  <li className="nav-item">
                    <NavLink className="nav-link" to={`/dashboard/report/${employee.current_employee.attributes.branch.id}/day`}>
                      <i className="ni ni-calendar-grid-58"/> Daily Report
                    </NavLink>
                  </li>

                  <li className="nav-item">
                    <NavLink className="nav-link" to={`/dashboard/report/${employee.current_employee.attributes.branch.id}/week`}>
                      <i className="ni ni-calendar-grid-58"/> Weekly Report
                    </NavLink>
                  </li>

                  <li className="nav-item">
                    <NavLink className="nav-link" to={`/dashboard/report/${employee.current_employee.attributes.branch.id}/month`}>
                      <i className="ni ni-calendar-grid-58"/> Monthly Report
                    </NavLink>
                  </li>
                </React.Fragment>
              }


            </ul>

            <hr className="my-3"/>

          </div>
        </div>
      </nav>
    )
  }
}