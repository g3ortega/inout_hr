import React from "react";

export default class EmployeeStateBadge extends React.Component {
  constructor(props) {
    super(props);
  }

  static getBadge(state) {
    switch (state) {
      case 'out':
        return 'bg-info';
      case 'working':
        return 'bg-success';
      case 'sick':
        return 'bg-danger';
      case 'vacations':
        return 'bg-warning';
      default:
        return 'bg-default';
    }
  }

  render() {
    const {status} = this.props;

    return (
      <React.Fragment>
        {status &&
        <span className="badge badge-dot mr-4">
          <i className={this.constructor.getBadge(status)}/> {status}
        </span>}
      </React.Fragment>
    )
  }
}