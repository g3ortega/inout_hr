import React from 'react';
import {observer, inject} from 'mobx-react';
import TopNavBar from './UI/Navbar';
import Sidebar from './UI/Sidebar';
import e from "./Employees";
import r from "./Reports"
import BranchesShow from "./Branches/Show";
import {Route, Switch} from "react-router-dom";

@inject("employee") @observer
export default class Dashboard extends React.Component {

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.props.employee.signIn()
  }

  render() {
    const { employee } = this.props;
    return (
      <div>
        { employee && employee.current_employee && employee.current_employee.attributes.role === 'manager' &&
        <Sidebar/>
        }
        <div className="main-content">
          <TopNavBar />

          <div className="header bg-gradient-primary pb-8 pt-5 pt-md-8">
            <div className="container-fluid">
              <div className="header-body">
                <div className="row">
                  <div className="col-md-6">
                    <div className="card card-stats mb-4 mb-xl-0">
                      <div className="card-body">
                        <div className="row">
                          <div className="col">
                            <h5 className="card-title text-uppercase text-muted mb-0">Time Tracked This Week</h5>
                          </div>
                          <div className="col-auto">
                            <div className="icon icon-shape bg-danger text-white rounded-circle shadow">
                              <svg className="svg-inline--fa fa-rocket fa-w-16" aria-hidden="true" focusable="false"
                                   data-prefix="fas" data-icon="rocket" role="img" xmlns="http://www.w3.org/2000/svg"
                                   viewBox="0 0 512 512" data-fa-i2svg="">
                                <path fill="currentColor"
                                      d="M505.05 19.1a15.89 15.89 0 0 0-12.2-12.2C460.65 0 435.46 0 410.36 0c-103.2 0-165.1 55.2-211.29 128H94.87A48 48 0 0 0 52 154.49l-49.42 98.8A24 24 0 0 0 24.07 288h103.77l-22.47 22.47a32 32 0 0 0 0 45.25l50.9 50.91a32 32 0 0 0 45.26 0L224 384.16V488a24 24 0 0 0 34.7 21.49l98.7-49.39a47.91 47.91 0 0 0 26.5-42.9V312.79c72.59-46.3 128-108.4 128-211.09.1-25.2.1-50.4-6.85-82.6zM384 168a40 40 0 1 1 40-40 40 40 0 0 1-40 40z" />
                              </svg>
                            </div>
                          </div>
                        </div>

                        <div className="h1 lead mb-0 mt-0 text-muted">
                          {employee && employee.current_employee &&
                          <div className="text-blue">
                            {employee.current_employee.attributes.current_week_time_tracked}
                          </div>
                          }
                        </div>

                      </div>
                    </div>
                  </div>

                  <div className="col-md-6">
                    <div className="card card-stats mb-4 mb-xl-0">
                      <div className="card-body">
                        <div className="row">
                          <div className="col">
                            <h5 className="card-title text-uppercase text-muted mb-0">Your Status</h5>
                            <span className="h1 font-weight-bold mb-0">
                              {employee && employee.current_employee &&
                                <div className="text-blue">
                                  {employee.current_employee.attributes.state}
                                </div>
                              }
                            </span>
                          </div>
                          <div className="col-auto">
                            <div className="icon icon-shape bg-danger text-white rounded-circle shadow">
                              <svg className="svg-inline--fa fa-code fa-w-20" aria-hidden="true" focusable="false"
                                   data-prefix="fas" data-icon="code" role="img" xmlns="http://www.w3.org/2000/svg"
                                   viewBox="0 0 640 512" data-fa-i2svg="">
                                <path fill="currentColor"
                                      d="M278.9 511.5l-61-17.7c-6.4-1.8-10-8.5-8.2-14.9L346.2 8.7c1.8-6.4 8.5-10 14.9-8.2l61 17.7c6.4 1.8 10 8.5 8.2 14.9L293.8 503.3c-1.9 6.4-8.5 10.1-14.9 8.2zm-114-112.2l43.5-46.4c4.6-4.9 4.3-12.7-.8-17.2L117 256l90.6-79.7c5.1-4.5 5.5-12.3.8-17.2l-43.5-46.4c-4.5-4.8-12.1-5.1-17-.5L3.8 247.2c-5.1 4.7-5.1 12.8 0 17.5l144.1 135.1c4.9 4.6 12.5 4.4 17-.5zm327.2.6l144.1-135.1c5.1-4.7 5.1-12.8 0-17.5L492.1 112.1c-4.8-4.5-12.4-4.3-17 .5L431.6 159c-4.6 4.9-4.3 12.7.8 17.2L523 256l-90.6 79.7c-5.1 4.5-5.5 12.3-.8 17.2l43.5 46.4c4.5 4.9 12.1 5.1 17 .6z" />
                              </svg>
                            </div>
                          </div>
                        </div>
                        <p className="mt-3 mb-0 text-muted text-sm">
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Switch>
            <Route path='/dashboard/settings' component={e.Edit} />
            <Route path='/dashboard/branches/:id' component={BranchesShow} />
            <Route path='/dashboard/branch/:branch_id/employees/:employee_id' component={e.Show} />
            <Route path='/dashboard/working/:branch_id' component={r.WhosWorking} />
            <Route path='/dashboard/report/:branch_id/:period' component={r.HoursLogged} />
          </Switch>
        </div>
      </div>
    );
  }
}
