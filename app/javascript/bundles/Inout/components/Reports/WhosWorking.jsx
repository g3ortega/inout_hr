import React from 'react';
import Api from '../../helpers/api';
import {Link} from "react-router-dom";

class WhosWorking extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      employees_by_status: null
    }
  }

  async componentDidMount() {
    const response = await Api.get(`/branches/${this.props.match.params.branch_id}}/reports/whos_working`);
    const status = await response.status;

    switch (status) {
      case 200:
        const body = await response.json();

        console.log(body);

        this.setState({
          employees_by_status: body
        });
        break;
      case 401:
        break;
    }
  }

  render() {
    return (
      <div className="col-md-12 mt-1">
        <div className="card shadow">
          <div className="card-header border-0">
            <div className="row align-items-center">
              <div className="col-4">
                <h3 className="mb-2">Employees</h3>
              </div>
              <div className="col-8 text-right">
              </div>
            </div>
          </div>
          <div className="col-12">
            <div className="row">
              <div id="story-card" className="card card-shadow col-3 sprint-card">
                <h3 className="text-center">Out</h3>
                {this.state.employees_by_status && this.state.employees_by_status.out
                  && this.state.employees_by_status.out.map((employee) => {
                  return(
                  <div className="card card-stats mb-3" key={employee.id}>
                    <div className="card-body text-left">
                      <h3>
                        <Link
                          to={`/dashboard/branch/${this.props.match.params.branch_id}/employees/${employee.id}`}>
                          {employee.name}
                        </Link>
                      </h3>
                    </div>
                  </div>)
                })}
              </div>

              <div id="story-card" className="card card-shadow col-3 sprint-card">
                <h3 className="text-center">Working</h3>
                {this.state.employees_by_status && this.state.employees_by_status.working
                  && this.state.employees_by_status.working.map((employee) => {
                  return(
                    <div className="card card-stats mb-3" key={employee.id}>
                      <div className="card-body text-left">
                        <h3>
                          <Link
                            to={`/dashboard/branch/${this.props.match.params.branch_id}/employees/${employee.id}`}>
                            {employee.name}
                          </Link>
                        </h3>
                      </div>
                    </div>)
                })}
              </div>

              <div id="story-card" className="card card-shadow col-3 sprint-card">
                <h3 className="text-center">Sick</h3>
                {this.state.employees_by_status && this.state.employees_by_status.sick
                  && this.state.employees_by_status.sick.map((employee) => {
                  return(
                    <div className="card card-stats mb-3" key={employee.id}>
                      <div className="card-body text-left">
                        <h3>
                          <Link
                            to={`/dashboard/branch/${this.props.match.params.branch_id}/employees/${employee.id}`}>
                            {employee.name}
                          </Link>
                        </h3>
                      </div>
                    </div>)
                })}
              </div>

              <div id="story-card" className="card card-shadow col-3 sprint-card">
                <h3 className="text-center">On Vacation</h3>
                {this.state.employees_by_status && this.state.employees_by_status.on_vacation &&
                  this.state.employees_by_status.on_vacation.map((employee) => {
                  return(
                    <div className="card card-stats mb-3" key={employee.id}>
                      <div className="card-body text-left">
                        <h3>
                          <Link
                            to={`/dashboard/branch/${this.props.match.params.branch_id}/employees/${employee.id}`}>
                            {employee.name}
                          </Link>
                        </h3>
                      </div>
                    </div>)
                })}
              </div>

            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default WhosWorking;