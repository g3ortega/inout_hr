import HoursLogged from './HoursLogged';
import WhosWorking from "./WhosWorking";

export default {
  HoursLogged,
  WhosWorking
};