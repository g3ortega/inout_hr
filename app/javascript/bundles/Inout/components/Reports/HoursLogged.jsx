import React from 'react';
import Api from '../../helpers/api'
import ReactTable from 'react-table';
import {Link} from 'react-router-dom';
import 'react-table/react-table.css'
import EmployeeStateBadge from "../UI/EmployeeStateBadge";

class HoursLogged extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      branch: {
        employees: []
      }
    };

    this.fetchEmployees = this.fetchEmployees.bind(this);
  }

  componentDidMount() {
    this.fetchEmployees()
  }

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.match.params.period !== this.props.match.params.period){
      this.fetchEmployees();
      return null;
    }
  }

  async fetchEmployees() {
    const params = this.props.match.params;
    const response = await Api.get(`/branches/${params.branch_id}/employees?log[period]=${params.period}`);

    const status = await response.status;

    if (status === 200) {
      const body = await response.json();

      this.setState({
        employees: body.data
      })
    }
  }

  render() {

    const columns = [{
      Header: 'Name',
      accessor: 'name',
      Cell: row => (<Link
        to={`/dashboard/branch/${row.original.branch_id}/employees/${row.original.id}`}>
        {row.original.attributes.name}
      </Link>)
    }, {
      Header: 'Email',
      accessor: 'attributes.email'
    }, {
      Header: 'State',
      accessor: 'attributes.aasm_state',
      Cell: row => (<EmployeeStateBadge status={row.original.attributes.aasm_state} />)
    }, {
      Header: 'Hours Logged',
      accessor: 'attributes.schedule_logs_by_period'
    }];

    return (
      <div className="container-fluid mt--7">
        <div className="row">
          <div className="col">
            <div className="card shadow">
              <div className="card-header border-0">
                <div className="row align-items-center">
                  <div className="col-4">
                    <h3 className="mb-0">Employees</h3>
                  </div>
                  <div className="col-12 text-right">
                  </div>
                </div>
              </div>

              <div className="col-12">
                <ReactTable
                  data={this.state.employees}
                  columns={columns}
                  filterable
                />
              </div>

            </div>
          </div>
        </div>
      </div>

    );
  }
}
export default HoursLogged;