import React from "react";
import {BrowserRouter as Router, Route} from "react-router-dom";
import {Provider} from 'mobx-react';

import Home from "./Home"
import Dashboard from "./Dashboard"
import Branch from "./Branches/Show";
import stores from '../stores';
import reports from './Reports'

import e from './Employees'

function Index() {
  return (
    <Provider {...stores}>
      <Router>
        <div>
          <Route exact path="/" component={Home} />
          <Route path="/dashboard" component={Dashboard} />
        </div>
      </Router>
    </Provider>
  );
}

export default Index;