import React from 'react';
import NewSession from './Sessions/New';
import { observer, inject } from 'mobx-react';

@inject('employee') @observer
export default class Home extends React.Component {

  constructor(props) {
    super(props);
  }

  componentWillMount() {
    this.props.employee.signIn()
  }

  render() {
    return (
      <div className="main-content">
        <div className="header bg-gradient-primary py-7 py-lg-8">
          <div className="container">
            <div className="header-body text-center mb-7">
              <div className="row justify-content-center">
                <div className="col-lg-5 col-md-6">
                  <h1 className="text-white">Welcome to InOut HR</h1>
                  <p className="text-lead text-light">Please Sign In with your Employee or Admin credentials.</p>
                </div>
              </div>
            </div>
          </div>
          <div className="separator separator-bottom separator-skew zindex-100">
            <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1"
                 xmlns="http://www.w3.org/2000/svg">
              <polygon className="fill-default" points="2560 0 2560 100 0 100" />
            </svg>
          </div>
        </div>

        <NewSession />

      </div>
    );
  }
}
