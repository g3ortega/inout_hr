import React from 'react';
import { observer, inject } from 'mobx-react';

@inject('employee') @observer
class New extends React.Component {

  submitForm = (e) => {
    e.preventDefault();

    const { employee } = this.props;

    employee.signIn(
      this.email.value,
      this.password.value,
    );
  };

  render() {
    return (
      <div className="container mt--8 pb-5">
        <div className="row justify-content-center">
          <div className="col-lg-5 col-md-7">
            <div className="card bg-secondary shadow border-0">
              <div className="card-body px-lg-5 py-lg-5">
                <div className="text-center text-muted mb-4">
                  <small>Sign in with credentials</small>
                </div>
                <form role="form" onSubmit={this.submitForm}>
                  <div className="form-group mb-3">
                    <div className="input-group input-group-alternative">
                      <div className="input-group-prepend">
                        <span className="input-group-text"><i className="ni ni-email-83" /></span>
                      </div>
                      <input className="form-control"
                             placeholder="Email" type="email"
                             ref={node => { this.email = node; }} />
                    </div>
                  </div>
                  <div className="form-group">
                    <div className="input-group input-group-alternative">
                      <div className="input-group-prepend">
                        <span className="input-group-text"><i className="ni ni-lock-circle-open" /></span>
                      </div>
                      <input className="form-control"
                             placeholder="Password" type="password"
                             ref={node => { this.password = node; }} />
                    </div>
                  </div>
                  <div className="custom-control custom-control-alternative custom-checkbox">
                    <input className="custom-control-input" id=" customCheckLogin" type="checkbox"/>
                    <label className="custom-control-label" htmlFor=" customCheckLogin">
                      <span className="text-muted">Remember me</span>
                    </label>
                  </div>
                  <div className="text-center">
                    <button type="submit" className="btn btn-primary my-4">Sign in</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default New;