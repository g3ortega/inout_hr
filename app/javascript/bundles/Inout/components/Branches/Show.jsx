import React from 'react';
import Api from '../../helpers/api'
import ReactTable from 'react-table';
import {Link} from 'react-router-dom';
import 'react-table/react-table.css'
import EmployeeStateBadge from "../UI/EmployeeStateBadge";

class Show extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      branch: {
        employees: []
      }
    }
  }

  async componentDidMount() {
    const response = await Api.get(`/branches/${this.props.match.params.id}`);

    const status = await response.status;

    if (status === 200) {
      const body = await response.json();
      this.setState({
        branch: body.data.attributes
      })
    }
  }

  render() {

    const columns = [{
      Header: 'Name',
      accessor: 'name',
      Cell: row => (<Link
        to={`/dashboard/branch/${row.original.branch_id}/employees/${row.original.id}`}>
        {row.original.name}
      </Link>)
    }, {
      Header: 'Address',
      accessor: 'address'
    }, {
      Header: 'Position',
      accessor: 'position'
    }, {
      Header: 'Email',
      accessor: 'email'
    }, {
      Header: 'State',
      accessor: 'aasm_state',
      Cell: row => (<EmployeeStateBadge status={row.original.aasm_state} />)
    }];

    return (
      <div className="container-fluid mt--7">
        <div className="row">
          <div className="col">
            <div className="card shadow">
              <div className="card-header border-0">
                <div className="row align-items-center">
                  <div className="col-4">
                    <h3 className="mb-0">Employees</h3>
                  </div>
                  <div className="col-12 text-right">
                  </div>
                </div>
              </div>

              <div className="col-12">
                <ReactTable
                  data={this.state.branch.employees}
                  columns={columns}
                  filterable
                />
              </div>

            </div>
          </div>
        </div>
      </div>

    );
  }
}
export default Show;