import { observable, action } from 'mobx';
import { createBrowserHistory } from 'history'

import Api from '../../helpers/api';

const history = createBrowserHistory({
  basename: '',
  forceRefresh: true
});

class Employee {
  sessions = '/sessions';
  employees = '/employees';

  @observable isLoading = false;
  @observable signedIn = false;
  @observable email = null;
  @observable current_employee = null;

  @action setIsLoading(status) {
    this.isLoading = status;
  }

  @action setSignedIn(status, current_employee) {
    this.signedIn = status;
    if (status && current_employee) {
      this.current_employee = current_employee;
    }
  }

  signIn(email = null, password = null) {
    const store = {
      authentication_token: localStorage.getItem('token'),
      email: localStorage.getItem('email'),
      current_employee: JSON.parse(localStorage.getItem('current_employee'))
    };

    if (store.email && store.authentication_token) {
      this.signInFromStorage(store.current_employee);
    } else if (email && password) {
      this.createSession(email, password);
    } else {
      this.signOut();
    }
  }

  @action async signInFromStorage(employee) {
    const response = await Api.get(this.sessions);
    const status = await response.status;

    if (status === 200) {
      this.current_employee = employee;
      this.signedIn = true;
      this.isLoading = false;

      if (history.location.pathname !== '/dashboard') {
        history.push('/dashboard');
      }

    } else {
      this.signOut();
    }
  }

  async createSession(email, password) {
    this.setIsLoading(true);

    const response = await Api.post(
      this.sessions,
      { email, password }
    );

    const status = await response.status;

    if (status === 201) {
      const body = await response.json();

      localStorage.setItem('token', body.authentication_token);
      localStorage.setItem('email', body.employee.data.attributes.email);
      localStorage.setItem('current_employee', JSON.stringify(body.employee.data));

      this.setIsLoading(false);
      this.setSignedIn(true, body.employee.data.attributes);

      if (history.location.pathname !== '/dashboard') {
        history.push('/dashboard');
      }
    } else {
      console.log('error');
    }
  }

  async destroySession() {
    this.setIsLoading(true);

    const response = await Api.delete(this.sessions);
    const status = await response.status;

    if (status === 200) {
      this.setIsLoading(false);
      this.signOut();
    }
  }

  @action signOut() {
    localStorage.removeItem('email');
    localStorage.removeItem('token');
    localStorage.removeItem('current_employee');

    this.employee = null;
    this.signedIn = false;
    this.isLoading = false;

    if (history.location.pathname !== '/') {
      history.push('/');
    }
  }
}

export default new Employee();