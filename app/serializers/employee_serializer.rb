# frozen_string_literal: true

class EmployeeSerializer
  include FastJsonapi::ObjectSerializer
  set_type :employee

  attributes :id, :name, :address, :phone_number, :emergency_number,
             :photo, :position, :email, :role, :aasm_state, :branch, :created_at, :updated_at

  attribute :state do |employee, _|
    employee.aasm_state.capitalize
  end

  attribute :current_week_time_tracked do |employee, _|
    employee.logged_time_by_period('week')
  end

  attribute :schedule_logs_by_period do |employee, params|
    if params.present? && params[:period]
      employee.logged_time_by_period(params[:period])
    end
  end
end
