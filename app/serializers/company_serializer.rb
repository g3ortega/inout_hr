class CompanySerializer
  include FastJsonapi::ObjectSerializer
  set_type :company

  attributes :name, :settings, :schedules, :weekly_workload
end