class BranchSerializer
  include FastJsonapi::ObjectSerializer
  set_type :branch

  attributes :name, :location

  attribute :employees do |branch, _|
    branch.employees.select('employees.id',
                            'employees.name',
                            'employees.address',
                            'employees.phone_number',
                            'employees.email',
                            'employees.position',
                            'employees.branch_id',
                            'employees.aasm_state')
  end

end