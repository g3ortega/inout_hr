class ScheduleLogSerializer
  include FastJsonapi::ObjectSerializer
  set_type :schedule_log

  attributes :start_time, :end_time, :log_type, :notes

  belongs_to :employee
end