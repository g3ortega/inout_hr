# frozen_string_literal: true

module Authenticable
  extend ActiveSupport::Concern

  included do
    before_action :authenticate_employee!
    before_action :set_branch
  end

  def execute_and_render_if_manager(error_message = "You're not authorized to access this")
    can_manage? ? yield : render(json: { message: error_message }, status: :unauthorized)
  end

  def execute_event_if_manager
    if can_manage?
      yield
    else
      render(json: {
               message: "You're not authorized to execute this event"
             }, status: :unauthorized)
    end
  rescue AASM::InvalidTransition => e
    render json: { message: e.message }, status: :bad_request
  end

  def can_manage?
    @branch.employees.where(role: %w[manager admin], id: current_employee.id).any?
  end

  private

  def set_branch
    @branch = current_employee.branch
  end

  def validate_permissions
    unless can_manage?
      render json: { message: 'You are not authorized' }, status: :unauthorized
    end
  end
end
