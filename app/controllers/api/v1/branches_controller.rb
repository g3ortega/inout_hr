# frozen_string_literal: true

class Api::V1::BranchesController < ApplicationController
  include Authenticable

  def index
    execute_and_render_if_manager do
      render json: BranchSerializer.new(Branch.includes(:employees, :branch_employees).all)
    end
  end

  def show
    execute_and_render_if_manager do
      render json: BranchSerializer.new(current_employee.branch)
    end
  end
end
