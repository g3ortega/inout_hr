# frozen_string_literal: true

class Api::V1::EmployeesController < ApplicationController
  include Authenticable
  before_action :set_employee, only: %i[clock_in clock_out got_sick on_vacation recovered]

  def index
    execute_and_render_if_manager do
      render json: EmployeeSerializer.new(Employee.all, params: {
                                            period: params.dig(:log, :period)
                                          })
    end
  end

  def show
    if can_manage?
      render json: EmployeeSerializer.new(@branch.employees.find(params[:id]))
    elsif current_employee&.id == params[:id].to_i
      render json: EmployeeSerializer.new(current_employee)
    else
      render json: { message: 'You are not authorized to see this employee' }, status: :unauthorized
    end
  rescue ActiveRecord::RecordNotFound
    render json: { message: "Record not found with ID: #{params[:id]}" }, status: :not_found
  end

  def create
    execute_and_render_if_manager("You're not allowed to create employees") do
      employee = Employee.new(employee_params)

      if employee.save
        render json: EmployeeSerializer.new(employee), status: :created
      else
        render json: { message: 'There was an error saving a new employee' }, status: :bad_request
      end
    end
  end

  def update
    employee = @branch.employees.find(params[:id])
    execute_and_render_if_manager('Only managers or admins can edit employees') do
      update_employee(employee)
    end
  rescue ActiveRecord::RecordNotFound
    render json: { message: 'Employee not found' }, status: :not_found
  end

  def destroy
    employee = @branch.employees.find(params[:id])
    execute_and_render_if_manager('Only managers or admins can delete employees') do
      if employee.destroy
        head :ok
      else
        render json: { message: 'Error while deleting' }, status: :internal_server_error
      end
    end
  rescue ActiveRecord::RecordNotFound
    render json: { message: 'Employee not found' }, status: :not_found
  end

  # RPC style aasm events declaration
  Employee.aasm.events.map(&:name).each do |event|
    define_method event do
      execute_event_if_manager do
        if @employee.send("#{event}!")
          render json: { message: "#{event} executed" }, status: :ok
        else
          render json: { message: 'Error' }, status: :bad_request
        end
      end
    end
  end

  private

  def set_employee
    @employee = @branch.employees.where(id: params[:employee_id]).first
  end

  def employee_params
    params.require(:employee).permit(:name, :address, :phone_number, :emergency_number,
                                     :photo, :position, :email, :password,
                                     :password_confirmation, :aasm_state, :branch_id)
  end

  def update_employee(employee)
    if employee.update!(employee_params)
      employee.reload
      render json: EmployeeSerializer.new(employee), status: :ok
    else
      render json: { message: 'There was an error saving a new employee' }, status: :bad_request
    end
  end
end
