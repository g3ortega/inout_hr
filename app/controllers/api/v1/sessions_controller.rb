class Api::V1::SessionsController < ApplicationController
  def show
    if current_employee
      render json: EmployeeSerializer.new(current_employee), status: :ok
    else
      head(:unauthorized)
    end
  end

  def create
    employee = Employee.where(email: params[:email]).first

    if employee&.valid_password?(params[:password])
      render json: {employee: EmployeeSerializer.new(employee),
                    authentication_token: employee.authentication_token},
             status: :created
    else
      head :unauthorized
    end
  end

  def destroy
    if invalidate_token
      head(:ok)
    else
      head(:unauthorized)
    end
  end

  private

  def invalidate_token
    current_employee&.authentication_token = nil
    current_employee&.save
  end
end