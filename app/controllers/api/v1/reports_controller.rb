# frozen_string_literal: true

class Api::V1::ReportsController < ApplicationController
  def whos_working
    branch = Branch.find(params[:branch_id])

    render json: branch.employees
                     .select('employees.id, employees.aasm_state, employees.name, employees.email')
                     .group_by(&:aasm_state)
  rescue ActiveRecord::NotFound
    render json: { message: 'Branch not found' }, status: :not_found
  end

  def schedule_logs_by_period
    group_by = params[:group_by]
    from = params[:from]
    to = params[:to]
    employee = Employee.find(params[:employee_id])

    render json: employee.logged_time_grouped(group_by, from, to)
  end

  def schedule_logs_by_branch
    group_by = params[:group_by]
    from = params[:from]
    to = params[:to]
    brach = Employee.find(params[:employee_id])
  end
end
