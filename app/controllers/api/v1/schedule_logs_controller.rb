class Api::V1::ScheduleLogsController < ApplicationController

  include Authenticable

  before_action :set_employee
  before_action :validate_permissions, only: %i[index]
  before_action :set_schedule_log, except: [:index]

  def index
    render json: ScheduleLogSerializer.new(@employee.schedule_logs)
  end

  def show
    render json: ScheduleLogSerializer.new(@schedule_log)
  end

  private

  def set_schedule_log
    @schedule_log = @employee.schedule_logs.find(params[:id])
  end

  def set_employee
    @employee = Employee.find(params[:employee_id])
  end

  def validate_permissions
    unless can_manage? || @employee.id == current_employee.id
      render json: {message: 'You are not authorized'}, status: :unauthorized
    end
  end

end