# == Schema Information
#
# Table name: branches
#
#  id         :bigint           not null, primary key
#  name       :string
#  location   :string
#  company_id :bigint
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Branch < ApplicationRecord
  belongs_to :company

  has_many :employees, dependent: :destroy

  validates_presence_of :name, :company
end


# employee.role ->
#
# admin
# manager
# employee
#
# delete -> employee.is_admin
# add -> employee.role
# remove -> branches_employees