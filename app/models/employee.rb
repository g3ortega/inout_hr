# frozen_string_literal: true

# == Schema Information
#
# Table name: employees
#
#  id                     :bigint           not null, primary key
#  name                   :string
#  address                :string
#  phone_number           :string
#  emergency_number       :string
#  photo                  :string
#  position               :string
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  authentication_token   :string(30)
#  aasm_state             :string           default("out")
#  role                   :string           default("employee")
#  branch_id              :bigint
#

class Employee < ApplicationRecord
  include AASM

  OUT = 'out'
  WORKING = 'working'
  SICK = 'sick'
  VACATIONS = 'vacations'

  STATES = [OUT, WORKING, SICK, VACATIONS].freeze

  acts_as_token_authenticatable
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :schedule_logs, dependent: :destroy
  belongs_to :branch

  validates_presence_of :name

  aasm do
    state :out, initial: true
    state :working, :sick, :on_vacation

    event :clock_in do
      before do
        active_schedule_log&.update(end_time: Time.now)
        schedule_logs.create(start_time: Time.now, log_type: 'working')
      end

      transitions from: :out, to: :working
    end

    event :clock_out do
      before do
        active_schedule_log&.update(end_time: Time.now)
      end

      transitions from: :working, to: :out
    end

    event :got_sick do
      before do
        active_schedule_log&.update(end_time: Time.now)
        schedule_logs.create(start_time: Time.now, log_type: 'sick')
      end

      transitions from: %i[out working], to: :sick
    end

    event :recovered do
      before do
        active_schedule_log('sick')&.update(end_time: Time.now)
      end

      transitions from: :sick, to: :out
    end

    event :went_on_vacation do
      before do
        schedule_logs.create(start_time: Time.now, log_type: 'sick')
      end

      transitions from: :out, to: :on_vacation
    end
  end

  def active_schedule_log(log_type = 'working')
    schedule_logs.where(end_time: nil, log_type: log_type).first
  end

  def logged_time_grouped(group_by, from, to)
    if group_by && from && to
      schedule_logs
        .where(start_time: from..to)
        .where.not(start_time: nil, end_time: nil)
        .send("group_by_#{group_by}", :created_at)
        .sum('schedule_logs.end_time - schedule_logs.start_time')
    end
  end

  def logged_time_by_period(period)
    format_single_grouped_value(logged_time_grouped(period, Time.now.send("beginning_of_#{period}"), Time.now))
  end

  private

  def format_single_grouped_value(value)
    value&.values&.first&.split('.')&.first
  end

end
