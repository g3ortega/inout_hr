# == Schema Information
#
# Table name: schedule_logs
#
#  id          :bigint           not null, primary key
#  start_time  :datetime
#  end_time    :datetime
#  employee_id :bigint
#  log_type    :string
#  notes       :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class ScheduleLog < ApplicationRecord
  belongs_to :employee

  validates_presence_of :log_type
end
