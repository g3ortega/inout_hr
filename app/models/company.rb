# == Schema Information
#
# Table name: companies
#
#  id              :bigint           not null, primary key
#  name            :string
#  settings        :jsonb
#  schedules       :jsonb
#  weekly_workload :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Company < ApplicationRecord
  has_many :branches

  validates_presence_of :name
end
