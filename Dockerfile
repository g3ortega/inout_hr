FROM ruby:2.6

LABEL Name=inout Version=0.0.1

RUN apt-get update && apt-get install -y apt-transport-https

RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

RUN apt-get update -y

RUN apt-get update -y && \
  apt-get install -y build-essential nodejs libpq-dev postgresql-client yarn jq imagemagick --fix-missing --no-install-recommends && \
  rm -rf /var/lib/apt/lists/*

ENV RAILS_ENV production
ENV RACK_ENV production

ENV RAILS_LOG_TO_STDOUT 1
ENV RAILS_SERVE_STATIC_FILES 1

WORKDIR /app

COPY Gemfile* ./
RUN gem install bundler rake
RUN bundle install

# throw errors if Gemfile has been modified since Gemfile.lock
RUN bundle config --global frozen 1

COPY package.json yarn.lock ./
RUN yarn install --check-files

COPY . .

RUN bundle exec rake --verbose --trace assets:precompile

EXPOSE 3000
CMD ["bundle", "exec", "rails", "server"]
    