# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'Sessions controller', type: :request do
  before { create(:employee, :with_branch) }

  path '/api/v1/sessions' do
    parameter 'X-Employee-Email', in: :header, type: :string
    let(:'X-Employee-Email') { Employee.last.email }

    parameter 'X-Employee-Token', in: :header, type: :string
    let(:'X-Employee-Token') { Employee.last.authentication_token }

    operation 'GET', summary: 'fetch session' do
      tags :sessions
      response 200, description: 'successful'
    end
  end

  path '/api/v1/sessions' do
    post summary: 'create' do
      produces 'application/json'
      consumes 'application/json'

      parameter 'body', in: :body, schema: { type: :object, properties: {
        email: { type: :string },
        password: { type: :string }
      } }
      let(:body) { { email: Employee.last.email, password: '12345678' } }

      response(201, description: 'session successfully created') do
        tags :sessions

        it 'check the employee session' do
          body = JSON.parse(response.body)
          expect(body['employee']['data']['id'].to_i).to eq(Employee.last.id)
        end

        capture_example
      end
    end
  end

  path '/api/v1/sessions' do
    parameter 'X-Employee-Email', in: :header, type: :string
    let(:'X-Employee-Email') { Employee.last.email }

    parameter 'X-Employee-Token', in: :header, type: :string
    let(:'X-Employee-Token') { Employee.last.authentication_token }

    operation 'DELETE', summary: 'fetch session' do
      tags :sessions
      response 200, description: 'successful'
    end
  end
end
