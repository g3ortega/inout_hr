# frozen_string_literal: true

require 'swagger_helper'

RSpec.describe 'Employees controller', type: :request do
  before do
    branch = create(:branch)

    5.times do
      create(:employee, branch: branch)
    end

    create(:employee, :manager, branch: branch)
  end

  path '/api/v1/branches/{branch_id}/employees' do
    parameter 'X-Employee-Email', in: :header, type: :string
    let(:'X-Employee-Email') { Employee.last.email }

    parameter 'X-Employee-Token', in: :header, type: :string
    let(:'X-Employee-Token') { Employee.last.authentication_token }

    parameter 'branch_id', in: :path, type: :integer
    let(:branch_id) { Employee.last.branch.id }

    operation 'GET', summary: 'Fetch Employees collection as Manager' do
      tags :employees

      response(200, description: 'employees fetched successfully') do
        capture_example
      end
    end
  end

  path '/api/v1/branches/{branch_id}/employees' do
    parameter 'X-Employee-Email', in: :header, type: :string
    let(:'X-Employee-Email') { Employee.last.email }

    parameter 'X-Employee-Token', in: :header, type: :string
    let(:'X-Employee-Token') { Employee.last.authentication_token }

    parameter 'branch_id', in: :path, type: :integer
    let(:branch_id) { Employee.last.branch.id }

    parameter 'body', in: :body, schema: { type: :object, properties: {
      name: { type: :string },
      email: { type: :string },
      password: { type: :string },
      password_confirmation: { type: :string }
    } }

    let(:body) do
      { employee: {
        name: Faker::Internet.name,
        email: Faker::Internet.email,
        password: '12345678',
        password_confirmation: '12345678',
        branch_id: Branch.last.id
      } }
    end

    operation 'POST', summary: 'Creates a New Employee as Manager' do
      tags :employees

      produces 'application/json'
      consumes 'application/json'

      response(201, description: 'employees successfully created') do
        capture_example
      end
    end
  end

  path '/api/v1/branches/{branch_id}/employees/{employee_id}/clock_in' do
    parameter 'X-Employee-Email', in: :header, type: :string
    let(:'X-Employee-Email') { Employee.last.email }

    parameter 'X-Employee-Token', in: :header, type: :string
    let(:'X-Employee-Token') { Employee.last.authentication_token }

    parameter 'branch_id', in: :path, type: :integer
    let(:branch_id) { Employee.last.branch.id }

    parameter 'employee_id', in: :path, type: :integer
    let(:employee_id) { Employee.last.id }

    operation 'POST', summary: 'Clock In Employee as Manager' do
      tags :employees

      response(200, description: 'clock in successfully started') do
        capture_example
      end
    end
  end

  path '/api/v1/branches/{branch_id}/employees/{employee_id}/clock_out' do
    parameter 'X-Employee-Email', in: :header, type: :string
    let(:'X-Employee-Email') { Employee.last.email }

    parameter 'X-Employee-Token', in: :header, type: :string
    let(:'X-Employee-Token') { Employee.last.authentication_token }

    parameter 'branch_id', in: :path, type: :integer
    let(:branch_id) { Employee.last.branch.id }

    parameter 'employee_id', in: :path, type: :integer
    let(:employee_id) { Employee.last.id }

    before do
      Employee.last.clock_in!
    end

    operation 'POST', summary: 'Clock Out Employee as Manager' do
      tags :employees

      response(200, description: 'clock out successfully executed') do
        capture_example
      end
    end
  end
end
