# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::EmployeesController, type: :controller do
  let(:branch) { create(:branch) }
  let(:employee) { create(:employee, branch: branch) }
  let(:manager) { create(:employee, :manager, branch: branch) }

  let(:params)  do
    {
      id: employee.id,
      branch_id: branch.id,
      format: :json
    }
  end

  describe 'with valid token' do
    describe 'with employee role' do
      before(:each) { auth_with_employee(employee) }

      describe 'REST actions' do
        it 'should respond successfully' do
          get :show, params: params
          expect(response).to be_successful
        end

        it "shouldn't allow access to index" do
          get :index, params: params
          expect(response).to be_unauthorized
        end

        it "shouldn't allow to create a new employee" do
          post :create, params: params
          expect(response).to be_unauthorized
        end

        it "shouldn't allow to edit employees" do
          put :update, params: params
          expect(response).to be_unauthorized
        end

        it "shouldn't allow to delete employees" do
          delete :destroy, params: params
          expect(response).to be_unauthorized
        end
      end

      describe 'AASM actions' do
        it "should'nt allow to 'clock in' employee" do
          post :clock_in, params: params.merge!(employee_id: employee.id)
          expect(response).to be_unauthorized
        end

        it "should'nt allow to 'clock out' employee" do
          post :clock_in, params: params.merge!(employee_id: employee.id)
          expect(response).to be_unauthorized
        end

        it "shouldn't allow to execute 'got_sick' event on employee" do
          post :got_sick, params: params.merge!(employee_id: employee.id)
          expect(response).to be_unauthorized
        end

        it "shouldn't allow to execute 'recovered' event on employee" do
          post :recovered, params: params.merge!(employee_id: employee.id)
          expect(response).to be_unauthorized
        end

        it "shouldn't allow to execute 'on_vacation' event on employee" do
          post :went_on_vacation, params: params.merge!(employee_id: employee.id)
          expect(response).to be_unauthorized
        end
      end
    end

    describe 'with manager role' do
      before(:each) { auth_with_employee(manager) }

      describe 'REST actions as manager' do
        it 'should respond successfully' do
          get :show, params: params
          expect(response).to be_successful
        end

        it 'should allow access to index' do
          get :index, params: params
          expect(response).to be_successful
        end

        it 'should allow create a new employee' do
          post :create, params: params.merge!(employee: { name: 'Foo',
                                                          email: 'foo@bar.com',
                                                          password: '12345678',
                                                          password_confirmation: '12345678',
                                                          branch_id: branch.id })

          expect(response).to have_http_status(201)
        end

        it 'should allow to edit employee' do
          test_employee = create(:employee, branch: branch)
          put :update, params: params.merge!(employee: { name: 'FooBar' }, id: test_employee.id)
          test_employee.reload
          expect(test_employee.name).to eq('FooBar')
        end

        it 'should allow delete employee' do
          test_employee = create(:employee, branch: branch)
          delete :destroy, params: params.merge!(id: test_employee.id)
          expect(response).to be_successful
        end
      end

      describe 'AASM actions as manager' do
        describe '#clock_in' do
          let!(:event_response) do
            post :clock_in, params: params.merge!(employee_id: employee.id)
            response
          end

          it 'should allow clock in employee' do
            expect(event_response).to be_successful
          end

          it 'should have a working status' do
            employee.reload
            expect(employee.aasm_state).to eq('working')
          end

          it 'should have an active schedule log' do
            employee.reload
            expect(employee.active_schedule_log.present?).to be_truthy
          end
        end

        describe '#clock_out' do
          let!(:event_response) do
            employee.clock_in!
            post :clock_out, params: params.merge!(employee_id: employee.id)
            response
          end

          it 'should allow clock out employee' do
            expect(event_response).to be_successful
          end

          it 'should have an out status' do
            employee.reload
            expect(employee.aasm_state).to eq('out')
          end

          it "shouldn't have an active schedule log" do
            employee.reload
            expect(employee.active_schedule_log.present?).to be_falsey
          end

          it 'should resque a wrong state transition' do
            post :clock_out, params: params.merge!(employee_id: employee.id)
            expect(response).to have_http_status(:bad_request)
          end
        end
      end
    end
  end
end
