# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::V1::SessionsController, type: :controller do
  let(:employee) { create(:employee, :with_branch) }

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new session' do
        post :create, params: { email: employee.email, password: '12345678' }
        expect(response).to have_http_status(:created)
      end

      it 'returns a valid token' do
        post :create, params: { email: employee.email, password: '12345678' }
        expect(JSON.parse(response.body).try(:[], 'authentication_token')).to eq(employee.authentication_token)
      end
    end

    context 'with invalid params' do
      it 'returns an unauthorized head when no params provided' do
        post :create, params: {}
        expect(response).to have_http_status(:unauthorized)
      end
    end
  end
end
