module AuthHelpers
  def auth_with_employee(employee)
    request.headers['X-Employee-Email'] = employee.email
    request.headers['X-Employee-Token'] = employee.authentication_token
  end

  def clear_token
    request.headers['X-Employee-Email'] = nil
    request.headers['X-Employee-Token'] = nil
  end
end