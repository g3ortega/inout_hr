FactoryBot.define do
  factory :branch do
    name { "MyString" }
    location { "MyString" }
    company { create(:company) }
  end
end
