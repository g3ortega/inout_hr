FactoryBot.define do
  factory :schedule_log do
    start_time { "2019-06-22 12:06:38" }
    end_time { "2019-06-22 12:06:38" }
    employee { nil }
    log_type { "MyString" }
    notes { "MyText" }
  end
end
