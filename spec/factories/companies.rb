FactoryBot.define do
  factory :company do
    name { Faker::Company.name }
    settings { "" }
    schedules { "" }
    weekly_workload { 1 }
  end
end
