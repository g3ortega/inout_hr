FactoryBot.define do
  factory :branch_employee do
    employee { nil }
    branch { nil }
    role { "MyString" }
  end
end
