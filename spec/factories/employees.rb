FactoryBot.define do
  factory :employee do
    name { Faker::Name.name }
    email { Faker::Internet.email }
    password { '12345678' }
    password_confirmation { '12345678' }
    address { Faker::Address.street_address }
    phone_number { Faker::PhoneNumber.phone_number }
    emergency_number { Faker::PhoneNumber.phone_number }
    position { Faker::Job.position }

    trait :with_branch do
      branch { create(:branch) }
    end

    trait :admin do
      role { 'admin' }
    end

    trait :manager do
      role { 'manager' }
    end
  end
end
