require 'rails_helper'

RSpec.describe Employee, type: :model do
  it { should respond_to(:name) }
  it { should respond_to(:address) }
  it { should respond_to(:phone_number) }
  it { should respond_to(:emergency_number) }
  it { should respond_to(:photo) }
  it { should respond_to(:email) }
  it { should respond_to(:position) }
  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }
  it { should validate_presence_of(:name) }

  it { should have_many(:schedule_logs) }

  let(:employee) { create(:employee, :with_branch) }

  it 'default role to be employee' do
    expect(employee.role).to eq('employee')
  end

  describe "aasm states" do
    it "should have 'out' as default state" do
      expect(employee).to have_state(:out)
    end

    it "should transition from 'out' to 'working' on 'clock in'" do
      expect(employee).to transition_from(:out).to(:working).on_event(:clock_in)
    end

    it "should transition from 'working' to 'out' on 'clock_out'" do
      expect(employee).to transition_from(:working).to(:out).on_event(:clock_out)
    end

    it "should transition from 'out' to 'sick' on 'got_sick'" do
      expect(employee).to transition_from(:out).to(:sick).on_event(:got_sick)
    end

    it "should transition from 'working' to 'sick' on 'got_sick'" do
      expect(employee).to transition_from(:working).to(:sick).on_event(:got_sick)
    end
  end

end
