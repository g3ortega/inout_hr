require 'rails_helper'

RSpec.describe ScheduleLog, type: :model do
  it { should respond_to(:start_time) }
  it { should respond_to(:end_time) }
  it { should respond_to(:employee_id) }
  it { should respond_to(:log_type) }
  it { should respond_to(:notes) }
end
