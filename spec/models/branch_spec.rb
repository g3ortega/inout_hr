require 'rails_helper'

RSpec.describe Branch, type: :model do
  it { should respond_to(:name) }
  it { should respond_to(:location) }
  it { should belong_to(:company) }
  it { should have_many(:employees) }

  it { should validate_presence_of(:name) }
end
