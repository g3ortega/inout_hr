require 'rails_helper'

RSpec.describe Company, type: :model do
  it { should respond_to(:name) }
  it { should respond_to(:settings) }
  it { should respond_to(:schedules) }
  it { should respond_to(:weekly_workload) }

  it { should have_many(:branches) }
end
