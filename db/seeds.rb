# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
ScheduleLog.delete_all
Employee.delete_all
Branch.delete_all
Company.delete_all

company = Company.create!(name: 'Runa')
branch = company.branches.create!(name: 'Runa MX', location: 'Mexico City')

25.times do
  employee = Employee.create!(name: Faker::Name.name,
                              email: Faker::Internet.email,
                              password: '12345678',
                              password_confirmation: '12345678',
                              address: Faker::Address.street_address,
                              phone_number: Faker::PhoneNumber.phone_number,
                              emergency_number: Faker::PhoneNumber.phone_number,
                              position: Faker::Job.position,
                              branch_id: branch.id)

  30.times do |n|
    employee.schedule_logs.create!(log_type: 'working',
                                   start_time: n.days.ago,
                                   end_time: n.days.ago + rand(4..9).hours)
  end
end

Employee.create!(name: 'John Manager',
                 email: 'manager@inout.com',
                 password: '12345678',
                 password_confirmation: '12345678',
                 address: Faker::Address.street_address,
                 phone_number: Faker::PhoneNumber.phone_number,
                 emergency_number: Faker::PhoneNumber.phone_number,
                 role: 'manager',
                 position: 'Manager',
                 branch_id: branch.id)
