class ModifyBranchStruture < ActiveRecord::Migration[5.1]
  def change
    drop_table :branch_employees
    add_column :employees, :role, :string, default: "employee"
    remove_column :employees, :is_admin, :boolean
    add_reference :employees, :branch
  end
end
