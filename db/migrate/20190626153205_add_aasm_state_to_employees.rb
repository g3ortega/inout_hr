class AddAasmStateToEmployees < ActiveRecord::Migration[5.1]
  def change
    add_column :employees, :aasm_state, :string, default: 'out'
  end
end
