class CreateBranchEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :branch_employees do |t|
      t.references :employee, foreign_key: true
      t.references :branch, foreign_key: true
      t.string :role

      t.timestamps
    end
  end
end
