# frozen_string_literal: true

class SetEmployeeAsDefaultRoleForBranchEmployees < ActiveRecord::Migration[5.1]
  def change
    change_column_default :branch_employees, :role, from: nil, to: 'employee'
  end
end
