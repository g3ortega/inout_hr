class AddAuthenticationTokenToEmployees < ActiveRecord::Migration[5.1]
  def change
    add_column :employees, :authentication_token, :string, limit: 30
    add_index :employees, :authentication_token, unique: true
  end
end
