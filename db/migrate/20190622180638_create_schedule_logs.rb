class CreateScheduleLogs < ActiveRecord::Migration[5.1]
  def change
    create_table :schedule_logs do |t|
      t.datetime :start_time
      t.datetime :end_time
      t.references :employee, foreign_key: true
      t.string :log_type
      t.text :notes

      t.timestamps
    end
  end
end
