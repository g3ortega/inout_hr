class CreateCompanies < ActiveRecord::Migration[5.1]
  def change
    create_table :companies do |t|
      t.string :name
      t.jsonb :settings
      t.jsonb :schedules
      t.integer :weekly_workload

      t.timestamps
    end
  end
end
