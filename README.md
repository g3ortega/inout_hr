# InOut HR

[![pipeline status](https://gitlab.com/g3ortega/inout_hr/badges/master/pipeline.svg)](https://gitlab.com/g3ortega/inout_hr/commits/master)

## Executing Locally

### Docker

    docker-compose up
    
Set up database and generate test data
    
    docker-compose exec inout rake db:setup

## Current DB Structure

![](erd.jpeg)



